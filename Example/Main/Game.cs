using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

namespace Example
{
    public class Game : Microsoft.Xna.Framework.Game
    {
        //Things you need but don't touch!
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        //The one Random Number Generator
        public static Random rand = new Random();

        //Size of your window!
        public const int WINDOWWIDTH = 800;
        public const int WINDOWHEIGHT = 600;

        //Colors!
        Color backgroundColor = Color.Black;

        //On-Screen-Text!
        SpriteFont font;

        //Background Music!
        Song music;

        //Dudes!
        AlwaysMovingDude amd;
        ComputerDude cd;
        KeyMoveDude kmd;
        ShootingDude sd;

        //Interface Stuff!
        Button button;
        Wall wall;

        //Items!
        Apple apple;

        //All the Textures!
        Texture2D amdTexture;
        Texture2D cdTexture;
        Texture2D kmdTexture;
        Texture2D sdTexture;
        Texture2D buttonTexture;
        Texture2D tsTexture;
        Texture2D appleTexture;
        Texture2D bulletTexture;
        Texture2D background;

        //Array of things to Draw!
        List<Drawable> listOfDrawables;

        public Game()
        {
            //Things you need but don't touch!
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        //This is where you change things about your window and mouse.
        protected override void Initialize()
        {
            IsMouseVisible = true;
            Window.AllowUserResizing = true;
            //graphics.IsFullScreen = true;
            graphics.PreferredBackBufferWidth = WINDOWWIDTH;
            graphics.PreferredBackBufferHeight = WINDOWHEIGHT;
            Window.Title = "DudeCity";

            //Things you need but don't touch!
            graphics.ApplyChanges();
            base.Initialize();
        }

        //This is where you load all Texture2D's, Songs, SoundEffects, and Spritefonts.
        protected override void LoadContent()
        {
            //Things you need but don't touch!
            spriteBatch = new SpriteBatch(GraphicsDevice);

            //Set sprite images for each drawable.
            amdTexture = (Content.Load<Texture2D>("amd"));
            cdTexture = (Content.Load<Texture2D>("cd"));
            kmdTexture = (Content.Load<Texture2D>("kmd"));
            sdTexture = (Content.Load<Texture2D>("sd"));
            bulletTexture= (Content.Load<Texture2D>("bullet"));
            buttonTexture = (Content.Load<Texture2D>("button"));
            tsTexture = (Content.Load<Texture2D>("wall"));
            appleTexture = (Content.Load<Texture2D>("apple"));

            background = Content.Load<Texture2D>("city");

            //Sets all the default positions, speeds, textures, etc.
            SetDefaults();
            
            //Sets spritefont
            font = Content.Load<SpriteFont>("Font");

            //Sets the background music
            music = Content.Load<Song>("Lights");
            MediaPlayer.Play(music);
            MediaPlayer.Volume = .3f;

        }

        protected void SetDefaults()
        {
            amd = new AlwaysMovingDude();
            amd.SetPosition(650, 249);
            amd.Speed = 1;
            amd.Texture = amdTexture;

            cd = new ComputerDude();
            cd.SetPosition(60, 200);
            cd.Texture = cdTexture;

            kmd = new KeyMoveDude();
            kmd.SetPosition(250, 310);
            kmd.Speed = 1;
            kmd.Texture = kmdTexture;

            sd = new ShootingDude();
            sd.SetPosition(400, 100);
            sd.Speed = 1;
            sd.BulletSpeed = 1;
            sd.Texture = sdTexture;
            sd.BulletTexture = bulletTexture;

            button = new Button();
            button.SetPosition(500, 0);
            button.Texture = buttonTexture;

            wall = new Wall();
            wall.SetPosition(500, 300);
            wall.Texture = tsTexture;

            apple = new Apple();
            apple.SetPosition(200, 500);
            apple.Texture = appleTexture;

            //Adds Drawables to a List!
            //If it's not in the list,
            //it doesn't exist!
            listOfDrawables = new List<Drawable>
            { 
                amd, 
                cd, 
                kmd, 
                sd, 
                button, 
                wall, 
                apple
            };
        }

        //This is where all your dudes are updated!
        //It runs at 60 fps if possible.
        //No spriteBatch or draw commands should be here.
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit by pressing Escape!
            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
                this.Exit();

            //Updates everything in the list! 
            //If it's not in the list, it doesn't get updated!
            foreach (Drawable thingsToUpdate in listOfDrawables) 
            {
                thingsToUpdate.Update();
            }

            //Cleans up anything that is in the list that is not alive.
            for (int index = (listOfDrawables.Count - 1); index >= 0; index--)
            {
                if (!listOfDrawables[index].IsAlive)
                {
                    listOfDrawables.RemoveAt(index);
                }
            }

            CheckCollisions();

            //Things you need but don't touch!
            base.Update(gameTime);
        }

        private void CheckCollisions()
        {
            //This checks for collisions between bullets and everything in our list!
            sd.CheckBulletCollision(listOfDrawables);

            //Cycles through all our drawables
            foreach (Drawable objectA in listOfDrawables)
            {
                //and compares them to every other drawable!
                foreach (Drawable objectB in listOfDrawables)
                {
                    //Stops objects from checking themselves!
                    if (objectA != objectB)
                    {
                        //Check Collision between current two objects!
                        objectA.CheckCollision(objectB);
                    }
                }
            }
        }

        //This is where all your dudes are drawn!
        //It runs at 60 fps if possible.
        //No update logic should be in this method.
        protected override void Draw(GameTime gameTime)
        {
            ////Things you need but don't touch!
            GraphicsDevice.Clear(backgroundColor);
            spriteBatch.Begin();
            spriteBatch.Draw(background, new Rectangle(0, 0, WINDOWWIDTH, WINDOWHEIGHT), Color.White);
            //This is the Text in the middle of the screen. You may move it, comment it out, change the text, etc.
            spriteBatch.DrawString(font, "DUDE CITY", new Vector2((WINDOWWIDTH/ 2)-100, WINDOWHEIGHT / 2), Color.GhostWhite);

            //Draw everything in the list here!
            //If it isn't in the list, it doesn't get drawn!
            foreach (Drawable thingsToDraw in listOfDrawables)
            {
                thingsToDraw.Draw(spriteBatch);
            }

            //Things you need but don't touch!
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
