﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;

namespace Example
{
    class KeyMoveDude : Drawable
    {
        protected override void CheckKeyboard()
        {
            PreviousPosition = Position;
            if (Keyboard.GetState().IsKeyDown(Keys.Up))
            {
                PositionY -= (int)(Speed);
            }

            if (Keyboard.GetState().IsKeyDown(Keys.Right))
            {
                PositionX += (int)(Speed);
            }

            if (Keyboard.GetState().IsKeyDown(Keys.Down))
            {
                PositionY += (int)(Speed);
            }

            if (Keyboard.GetState().IsKeyDown(Keys.Left))
            {
                PositionX -= (int)(Speed);
            }
        }
    }
}
