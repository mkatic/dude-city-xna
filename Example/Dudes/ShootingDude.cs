﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Example
{
    class ShootingDude : Drawable
    {
        Texture2D bulletSprite;

        bool isKeyDown = false;

        double bulletSpeed;

        List<Bullet> listOfBullets = new List<Bullet>();

        public override void Update()
        {
            PreviousPosition = Position;
            //Updates every bullet in the list.
            foreach( Bullet bullet in listOfBullets)
            {
                bullet.Update();
            }

            //Cleans up any bullets that are dead or drifted off screen.
            Cleanup();

            base.Update();
        }
        public override void Draw(SpriteBatch spriteBatch)
        {
            foreach (Bullet bullet in listOfBullets)
            {
                bullet.Draw(spriteBatch);
            }
            base.Draw(spriteBatch);
        }
        public void Cleanup()
        {
            for (int index = listOfBullets.Count - 1; index >= 0; index--)
            {
                if (!listOfBullets[index].IsAlive)
                {
                    listOfBullets.RemoveAt(index);
                }
            }
        }

        //Checks for keyboard commands. Change the Keys.W to change what key.
        protected override void CheckKeyboard()
        {            
            if (Keyboard.GetState().IsKeyDown(Keys.W))
            {
                PositionY -= ((int)Speed);
            }
            if (Keyboard.GetState().IsKeyDown(Keys.D))
            {
                PositionX += ((int)Speed);
            }
            if (Keyboard.GetState().IsKeyDown(Keys.S))
            {
                PositionY += ((int)Speed);
            }
            if (Keyboard.GetState().IsKeyDown(Keys.A))
            {
                PositionX -= ((int)Speed);
            } 
           CheckKeyboardShoot();
        }

        //Checks to see if the keys to shoot have been pressed and shoots if the key has been released. 
        protected void CheckKeyboardShoot()
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Up) && (!isKeyDown))
            {
                Bullet bullet = new Bullet();
                bullet.SetPosition(PositionX, PositionY);
                bullet.Texture = bulletSprite;
                bullet.Speed = bulletSpeed;
                bullet.VelocityY = -1;
                listOfBullets.Add(bullet);
            }
            else if (Keyboard.GetState().IsKeyDown(Keys.Right) && (!isKeyDown))
            {
                Bullet bullet = new Bullet();
                bullet.SetPosition(PositionX, PositionY);
                bullet.Texture = bulletSprite;
                bullet.Speed = bulletSpeed;
                bullet.VelocityX = 1;
                listOfBullets.Add(bullet);
            }
            else if (Keyboard.GetState().IsKeyDown(Keys.Down) && (!isKeyDown))
            {
                Bullet bullet = new Bullet();
                bullet.SetPosition(PositionX, PositionY);
                bullet.Texture = bulletSprite;
                bullet.Speed = bulletSpeed;
                bullet.VelocityY = 1;
                listOfBullets.Add(bullet);
            }
            else if (Keyboard.GetState().IsKeyDown(Keys.Left) && (!isKeyDown))
            {
                Bullet bullet = new Bullet();
                bullet.SetPosition(PositionX, PositionY);
                bullet.Texture = bulletSprite;
                bullet.Speed = bulletSpeed;
                bullet.VelocityX = -1;
                listOfBullets.Add(bullet);
            }
            else
                isKeyDown = false;

            if (Keyboard.GetState().IsKeyDown(Keys.Up))
            {
                isKeyDown = true;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Down))
            {
                isKeyDown = true;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Left))
            {
                isKeyDown = true;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Right))
            {
                isKeyDown = true;
            }
        }
        public void CheckBulletCollision(List<Drawable> listOfDrawables)
        {
            //Cycles through every single bullet
            foreach (Bullet bullet in listOfBullets)
            {
                //and checks for collision against every drawable.
                foreach (Drawable drawable in listOfDrawables)
                {
                    //Doesn't destroy the shooter!
                    if (drawable != this) 
                    {
                        //Does kill the bullet and the object!
                        if (bullet.CheckCollision(drawable))
                        {
                            if (!(drawable is Wall))
                            {
                                drawable.IsAlive = false;
                            }
                            bullet.IsAlive = false;
                        }
                    }
                }
            }
        }

        public Texture2D BulletTexture
        {
            get { return bulletSprite; }
            set { bulletSprite = value; }
        }
        public double BulletSpeed
        {
            get { return bulletSpeed; }
            set { bulletSpeed = value; }
        }
    }
}
