﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;

namespace Example
{
    class AlwaysMovingDude : Drawable
    {
        //Should only ever be change from -1 to +1
        int directionX = 0;
        int directionY = 1;

        public override void Update()
        {
            PreviousPosition = Position;
            PositionX += (int)(directionX * Speed);
            PositionY += (int)(directionY * Speed);
            base.Update();
           
            
        }
        protected override void CheckKeyboard()
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Up))
            {
                directionY = -1;
                directionX = 0;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Right))
            {
                directionX = 1;
                directionY = 0;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Down))
            {
                directionY = 1;
                directionX = 0;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Left))
            {
                directionX = -1;
                directionY = 0;
            }
        }
        override protected void CheckOutOfBounds()
        {
            if (PositionX < -Width / 2)
                PositionX = Game.WINDOWWIDTH - Width / 2;
            if (PositionX > Game.WINDOWWIDTH - Width / 2)
                PositionX = -Width / 2;
            if (PositionY < -Height / 2)
                PositionY = Game.WINDOWHEIGHT - Height / 2;
            if (PositionY > Game.WINDOWHEIGHT - Height / 2)
                PositionY = -Height / 2;
        }
    }
}
