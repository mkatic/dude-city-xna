﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

namespace Example
{
    class Drawable
    {
        MouseState current, last;
        Texture2D drawableSprite;
        Rectangle spriteBounds;
        Color tint = Color.White;
        Vector2 position;
        Vector2 previousPosition = new Vector2();

        double speed = 0;
        float rotation = 0;
        float scale = 1.0f;

        bool isVisable = true;
        bool isAlive = true;

        //This is how the object is updated(movement, shooting, etc.)
        virtual public void Update()
        {
            CheckKeyboard();
            CheckOutOfBounds();
            CheckMouse(); 
        }

        //This is how the object is drawn to the screen.
        virtual public void Draw(SpriteBatch spriteBatch)
        {
            if (isVisable)
            {
                spriteBatch.Draw(drawableSprite, new Vector2((PositionX+Width/2), (PositionY+Height/2)), new Rectangle(0, 0, Width, Height),
                    Color.White, rotation,new Vector2(Width/2, Height/2),scale, SpriteEffects.None, 1.0f);
            }
        }

        //Called when a drawable is clicked on.
        virtual public void ClickedOn()
        {
            //Do nothing at drawable level.
        }

        //Checks to see if an object collides with another object and returns true or false.
        virtual public bool CheckCollision(Drawable otherDrawable)
        {
            if (spriteBounds.Intersects(otherDrawable.SpriteBounds))
                return true;
            else
                return false;
        }

        //Checks the edges of the screen then does the logic within.
        virtual protected void CheckOutOfBounds()
        {
            if (spriteBounds.X < 0)
                spriteBounds.X = 0;
            if (spriteBounds.X + spriteBounds.Width > Game.WINDOWWIDTH)
                spriteBounds.X = Game.WINDOWWIDTH - spriteBounds.Width;
            if (spriteBounds.Y < 0)
                spriteBounds.Y = 0;
            if (spriteBounds.Y + spriteBounds.Height > Game.WINDOWHEIGHT)
                spriteBounds.Y = Game.WINDOWHEIGHT - spriteBounds.Height;
        }

        virtual protected void CheckKeyboard()
        {
            //Do nothing at drawable level.
        }

        //Checks to see if the mouse has been clicked.
        //If  it has been, then it checks to see what it clicked on.
        virtual protected void CheckMouse()
        {
            current = Mouse.GetState();
            if (Mouse.GetState().LeftButton == ButtonState.Pressed&&last!=current)
            {
                int mouseX = Mouse.GetState().X;
                int mouseY = Mouse.GetState().Y;
                if (spriteBounds.Contains(mouseX, mouseY))
                {
                    ClickedOn();
                }
            }
            last = current;
        }

        //Changes the position of this object.
        public void SetPosition(int xPosition, int yPosition)
        {
            spriteBounds.X = xPosition;
            spriteBounds.Y = yPosition;
        }
        virtual public void SetPreviousPosition()
        {
            spriteBounds.X = (int)previousPosition.X;
            spriteBounds.Y = (int)previousPosition.Y;
        }

        //This is where all your property methods are located. 
        //They shouldn't need to be changed.
        #region Properties
        public bool IsVisable
        {
            get { return isVisable; }
            set { isVisable = value; }
        }
        public bool IsAlive
        {
            get { return isAlive; }
            set { isAlive = value; }
        }
        public int PositionX
        {
            get { return spriteBounds.X; }
            set { spriteBounds.X = value; }
        }
        public int PositionY
        {
            get { return spriteBounds.Y; }
            set { spriteBounds.Y = value; }
        }
        public double Speed
        {
            get { return speed; }
            set { speed = value; }
        }
        public Texture2D Texture
        {
            get { return drawableSprite; }
            set
            {
                drawableSprite = value;
                spriteBounds = new Rectangle((int)PositionX, (int)PositionY, drawableSprite.Width, drawableSprite.Height);
            }
        }
        public Rectangle SpriteBounds
        {
            get { return spriteBounds; }
        }
        public int Width
        {
            get { return spriteBounds.Width; }
        }
        public int Height
        {
            get { return spriteBounds.Height; }
        }
        public float Scale
        {
            get { return scale; }
            set { scale = value; }
        }
        public float Rotation
        {
            get { return rotation; }
            set { rotation = value; }
        }
        public Vector2 Position
        {
            get
            {
                position.X = spriteBounds.X;
                position.Y = spriteBounds.Y;
                return position;
            }
            set { Position = value; }
        }
        public Vector2 PreviousPosition
        {
            get { return previousPosition; }
            set { previousPosition = value; }
        }
        #endregion
    }
}