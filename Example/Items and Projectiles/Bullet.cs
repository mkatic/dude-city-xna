﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

namespace Example
{
    class Bullet : Drawable
    {
        int directionX = 0;
        int directionY = 0;

        public override void Update()
        {
            PreviousPosition = Position;
            PositionX += (int)(directionX * Speed);
            PositionY += (int)(directionY * Speed);
            base.Update();
        }
        protected override void CheckOutOfBounds()
        {
            if (PositionX + SpriteBounds.Width < 0 || PositionX > Game.WINDOWWIDTH ||
                PositionY + SpriteBounds.Height < 0 || PositionY > Game.WINDOWWIDTH)
            {
                IsAlive = false;
            }
        }
        public int VelocityX
        {
            get { return directionX; }
            set { directionX = value; }
        }
        public int VelocityY
        {
            get { return directionY; }
            set { directionY = value; }
        }
    }
}