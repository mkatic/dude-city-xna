﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Example
{
    class ComputerDude : Drawable
    {
        double lastRandomMoveTime;
        int direction; 
        public ComputerDude()
        {
            lastRandomMoveTime = DateTime.Now.Second;
            direction = Game.rand.Next(0, 8);
            Speed = Game.rand.Next(2,7);
        }
        public override void Update()
        {
            if (DateTime.Now.Second > (lastRandomMoveTime + 3))
            {
                ChangeDirection();
            }
            switch (direction)
            {
                case 0:
                    PositionX += (int)Speed;
                    break;
                case 1:
                    PositionX -= (int)Speed;
                    break;
                case 2:
                    PositionY += (int)Speed;
                    break;
                case 3:
                    PositionY -= (int)Speed;
                    break;
                case 4:
                    PositionX += (int)Speed;
                    PositionY += (int)Speed;
                    break;
                case 5:
                    PositionX += (int)Speed;
                    PositionY -= (int)Speed;
                    break;
                case 6:
                    PositionX -= (int)Speed;
                    PositionY += (int)Speed;
                    break;
                case 7:
                    PositionX -= (int)Speed;
                    PositionY -= (int)Speed;
                    break;
            }
            base.Update();
        }
        override public void SetPreviousPosition()
        {
            base.Update();
            ChangeDirection();
        }
        protected void ChangeDirection()
        {
            direction=Game.rand.Next(8);
            lastRandomMoveTime = DateTime.Now.Second;
            Speed = Game.rand.Next(2, 7);
        }
        override protected void CheckOutOfBounds()
        {
            if (PositionX < 0)
            {
                PositionX = 0;
                ChangeDirection();
            }
            if (PositionX + SpriteBounds.Width > Game.WINDOWWIDTH)
            {
                PositionX = Game.WINDOWWIDTH - SpriteBounds.Width;
                ChangeDirection();
            }
            if (PositionY < 0)
            {
                PositionY = 0;
                ChangeDirection();
            }
            if (PositionY + SpriteBounds.Height > Game.WINDOWHEIGHT)
            {
                PositionY = Game.WINDOWHEIGHT - SpriteBounds.Height;
                ChangeDirection();
            }
        }
    }
}
