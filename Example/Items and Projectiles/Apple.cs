﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Example
{
    class Apple : Drawable
    {
        public override bool CheckCollision(Drawable otherDrawable)
        {
            //This is where you add logic for what happens when the apple is hit.
            if (base.CheckCollision(otherDrawable))
            {
                IsAlive = false;
                return true;
            }
            else
                return false;
        }
    }
}
