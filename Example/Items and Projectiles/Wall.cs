﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Example
{
    class Wall : Drawable
    {
        public override bool CheckCollision(Drawable otherDrawable)
        {
            //This is where you add logic for what happens when the wall is hit.
            if (base.CheckCollision(otherDrawable))
            {
                otherDrawable.SetPreviousPosition();
                return true;
            }
            else
                return false;
        }
    }
}
