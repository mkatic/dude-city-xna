﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Example
{
    class Button : Drawable
    {
        public override void ClickedOn()
        {
            //Do button functions!
            IsVisable = !IsVisable;
        }
    }
}
